package terms;

import java.util.ArrayList;
import java.util.Map;

public class print {
	public static void format_print_wc(ArrayList<Map.Entry<String, Integer>> result) {
		for (int i = 0; i < Math.min(25, result.size()); i++) {
			System.out.println(result.get(i).getKey() + " - " + result.get(i).getValue());
		}
	}

	public static void format_print_kwic(ArrayList<Map.Entry<String, String>> result) {
		for (int i = 0; i < result.size(); i++) {
			System.out.println(result.get(i).getValue());
		}
	}
}
