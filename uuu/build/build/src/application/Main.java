package application;

import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import terms.SortMap;
import terms.readin;
import terms.stopwords;

public class Main extends Application {

	private Map<String, String> KW = new IdentityHashMap<String, String>();
	private stopwords stop;
	private readin read;
	public ArrayList<Map.Entry<String, String>> result;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		primaryStage.setTitle("KWIC");
		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
//		grid.setHgap(20);
//		grid.setVgap(10);
//		grid.setPadding(new Insets(0, 0, 0, 0));

		Text scenetitle = new Text("KWIC");
		scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
		grid.add(scenetitle, 50, 50, 2, 1);

		Label ignore = new Label("Ignore word:");
		grid.add(ignore, 50, 100);

		TextArea ignoreword = new TextArea();
		grid.add(ignoreword, 50, 150);

		Label title = new Label("Title:");
		grid.add(title, 50, 200);

		TextArea titleword = new TextArea();
		grid.add(titleword, 50, 250);

		Button btn = new Button("submit");
		HBox hbBtn = new HBox(10);
		hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
		hbBtn.getChildren().add(btn);
		grid.add(hbBtn, 50, 300);

		final TextArea actiontarget = new TextArea();
		actiontarget.setEditable(false);
		grid.add(actiontarget, 50, 400);

		btn.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent e) {
				stop = new stopwords(ignoreword.getText());
				read = new readin(titleword.getText());
				KW = new IdentityHashMap<String, String>();
				keywords_extraction();
				result = SortMap.Sort3(KW);
				// print.format_print_kwic(result);
				String output = "";
				for (int i = 0; i < result.size(); i++) {
					output = output + result.get(i).getValue() + "\n";
				}
				actiontarget.setText(output);
			}
		});

		Scene scene = new Scene(grid, 400, 600);
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	public void keywords_extraction() {
		int lineNum = 0;
		for (String sen : read.sentence) {
			lineNum += 1;
			String[] words = sen.toLowerCase().split(" ");
			for (int i = 0; i < words.length; i++) {
				if (stop.sp.contains(words[i])) {
					continue;
				} else {
					List<String> nl = new ArrayList<String>();
					int index = i;
					for (int j = 0; j < words.length; j++) {
						if (j == i) {
							nl.add(words[j].toUpperCase());
						} else {
							nl.add(words[j]);
						}
					}
					String s1 = String.valueOf(lineNum);
					String s2 = String.valueOf(index);
					String newline = String.join(" ", nl);
					KW.put(nl.get(index) + s1 + s2, newline);
				}
			}
		}
	}
}
