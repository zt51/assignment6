package terms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;
import java.util.Map.Entry;

public class SortMap {
	// sort the words using the frequency
	public static ArrayList<Map.Entry<String, Integer>> Sort2(Map<String, Integer> oldmap) {

		ArrayList<Map.Entry<String, Integer>> list = new ArrayList<Map.Entry<String, Integer>>(oldmap.entrySet());

		Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
			@Override
			public int compare(Entry<String, Integer> o1, Entry<String, Integer> o2) {
				if (o2.getValue() - o1.getValue() == 0) {
					return o1.getKey().compareTo(o2.getKey());
				} else {
					return o2.getValue() - o1.getValue();
				}
			}
		});
		return list;
	}

	public static ArrayList<Map.Entry<String, String>> Sort3(Map<String, String> oldmap) {

		ArrayList<Map.Entry<String, String>> list = new ArrayList<Map.Entry<String, String>>(oldmap.entrySet());

		Collections.sort(list, new Comparator<Map.Entry<String, String>>() {
			@Override
			public int compare(Entry<String, String> o1, Entry<String, String> o2) {
				return o1.getKey().compareTo(o2.getKey());
			}
		});
		return list;
	}
}
